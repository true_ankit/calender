$(document).ready(function () {
  var dt = new Date()
  var stringMonth = dt.toLocaleString("default", { month: "long" })
  var month = dt.getMonth()
  var day = dt.getDate()

  const settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://public-holiday.p.rapidapi.com/2022/US",
    "method": "GET",
    "headers": {
      "X-RapidAPI-Key": "f2024c8741mshdef02ddf2ec6c25p1b3b14jsnfd06488cae9c",
      "X-RapidAPI-Host": "public-holiday.p.rapidapi.com"
    }
  };

  Fancybox.bind('[data-fancybox=""]', {
    // Do not create a gallery
    groupAttr: null,
    // Disable drag to close guesture
    dragToClose: false,

  })

  function addHoliday(dt) {
    $.ajax(settings).done(function (response) {
      $.each(response, function (index, item) {
        var date = new Date(item.date)
        if (date.getMonth() === dt.getMonth()) {
          var $day = $("#" + date.getDate())
          $day.addClass("holiday")
          $day.attr({
            "data-fancybox": "",
            "data-src": "#holiday-" + date.getDate()
          })
          $(".days").append("<div class='holiday-info' id='holiday-" + date.getDate() + "'></div>")
          $("#holiday-" + date.getDate()).append("<h3>" + item.localName + "</h3>")
        }
      })
    });
  }
  function addHolidayClassToSat(days) {
    days.each(function (index) {
      if ((index + 1) % 7 == 0) {
        $(this).addClass('holiday')
      }
    })
  }

  function add_days(dt) {

    var endDate = new Date(
      dt.getFullYear(),
      dt.getMonth() + 1,
      0
    ).getDate()

    var prevEndDate = new Date(
      dt.getFullYear(),
      dt.getMonth(),
      0
    ).getDate()
    dt.setDate(1)
    for (i = dt.getDay(); i > 0; i--) {
      $(".days").append("<div class='prev-date'>" + (prevEndDate - i + 1) + "</div>")
    }
    for (i = 1; i <= endDate; i++) {
      if (i == day && dt.getMonth() === month) {
        $(".days").append("<div class='current' id=" + i + ">" + i + "</div>")
      } else {
        $(".days").append("<div id=" + i + ">" + i + "</div>")
      }
    }
    addHoliday(dt)
    addHolidayClassToSat($(".days div"))
  }

  $(".month p").text(dt.toDateString())
  $(".month h2").text(stringMonth)
  add_days(dt)


  $(".next").click({ dt: dt }, function () {
    $(".days").empty()
    if (dt.getMonth() + 1 <= 11) {
      dt.setMonth(dt.getMonth() + 1)
    } else {
      dt.setMonth(0)
    }
    add_days(dt)
    $(".month h2").text(dt.toLocaleString("default", { month: "long" }))
  })

  $(".prev").click({ dt: dt }, function () {
    $(".days").empty()
    $(".days").empty()
    if (dt.getMonth() - 1 >= 0) {
      dt.setMonth(dt.getMonth() - 1)
    } else {
      dt.setMonth(11)
    }
    add_days(dt)
    $(".month h2").text(dt.toLocaleString("default", { month: "long" }))
  })

});